FROM alpine:latest

RUN apk add --no-cache doxygen graphviz ttf-cantarell

WORKDIR /opt/prj
VOLUME [ "/opt/prj" ]

CMD [ "doxygen" ]
